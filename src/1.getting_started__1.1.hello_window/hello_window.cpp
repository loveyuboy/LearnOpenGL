#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

void processInput(GLFWwindow* window);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);

int main()
{
	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LernOpenGL", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to crate glfw window!" << std::endl;
		return -1;
	}
	// 绑定当前窗口
	glfwMakeContextCurrent(window);
	// 设置窗口大小变化的回调方法
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	
	// glad: 加载所有OpenGL函数指针
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// 渲染循环
	while (!glfwWindowShouldClose(window))
	{
		// 处理输入
		processInput(window);
		
		// 渲染
		glClearColor(.2f, .3f, .3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// 交换颜色缓冲区
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	// 关闭
	glfwTerminate();
	return 0;
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window,GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

// GLFWwindow 视口尺寸变化的时候回调
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// 同步的修改 glViewport
	glViewport(0, 0, width, height);
}